# rstatsmethods <a href='https:/crumplab.github.io/rstatsmethods'><img src='man/figures/logo.png' align="right" height="120.5" width="120.5" /></a>

<!-- badges: start -->
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
<!-- badges: end -->

## PSYC 7765/66 Statistical Methods Application I

This is the course website for PSYC 7765/66: Statistical Methods Applications I (Fall 2021), offered through the Experimental Psychology Master’s program, Department of Psychology, Brooklyn College of CUNY. 

## PSYC 7709G: Using R for Reproducible Research

Offered in Spring 2022, and continues from the above

Instructor: Matthew Crump
[mcrump@brooklyn.cuny.edu](mcrump@brooklyn.cuny.edu)

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
